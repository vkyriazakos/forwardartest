package kyriazakos.forwardgame.forwardartest;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.mapbox.mapboxsdk.Mapbox;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;
import android.location.Location;
import com.mapbox.mapboxsdk.geometry.LatLng;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerMode;
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin;
import com.mapbox.mapboxsdk.style.layers.PropertyFactory;
import com.mapbox.mapboxsdk.style.layers.SymbolLayer;
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource;
import com.mapbox.services.android.telemetry.location.LocationEngine;
import com.mapbox.services.android.telemetry.location.LocationEngineListener;
import com.mapbox.services.android.telemetry.location.LocationEnginePriority;
import com.mapbox.services.android.telemetry.location.LocationEngineProvider;
import com.mapbox.services.android.telemetry.permissions.PermissionsListener;
import com.mapbox.services.android.telemetry.permissions.PermissionsManager;
import com.mapbox.services.commons.geojson.Feature;
import com.mapbox.services.commons.geojson.FeatureCollection;
import com.mapbox.services.commons.geojson.Point;
import com.shashank.sony.fancydialoglib.FancyAlertDialog;
import com.shashank.sony.fancydialoglib.FancyAlertDialogListener;
import com.shashank.sony.fancydialoglib.Icon;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import de.hdodenhof.circleimageview.CircleImageView;

public class MapboxActivity extends AppCompatActivity implements LocationEngineListener, PermissionsListener {

    private static final String MARKER_SOURCE = "markers-source";
    private static final String MARKER_STYLE_LAYER = "markers-style-layer";
    private static final String MARKER_NAME = "bomb_marker";

    private PermissionsManager permissionsManager;
    private MapboxMap mapBoxMap;
    private LocationLayerPlugin locationLayerPlugin;
    private LocationEngine locationEngine;
    private String provider;
    private MapView mapView;

    private boolean sillyButton1 = true, sillyButton2 = true;// silly booleans for image change

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Mapbox.getInstance(this, "pk.eyJ1Ijoidmt5cmlhemFrb3MiLCJhIjoiY2pmMnJwZzdtMHJ0NDJ3cTdzem16NXVjNyJ9.EX8pu58PGuEn9E6MWPxONw");
        setContentView(R.layout.activity_mapbox);

        // Not sure if needed.
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // Make menu invisible
        RelativeLayout menu = (RelativeLayout) findViewById(R.id.btn_menu);
        menu.setVisibility(View.GONE);

        // Initialize Mapview
        mapView = (MapView) findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                MapboxActivity.this.mapBoxMap = mapboxMap;

                Bitmap icon = BitmapFactory.decodeResource(
                        MapboxActivity.this.getResources(), R.drawable.bomb_marker);
                mapboxMap.addImage(MARKER_NAME, icon);
                addMarkers();

                // Also instantiate location layer
                startLocationLayer();

                // Also slide in menu button [sloppy]
                RelativeLayout menu = (RelativeLayout) findViewById(R.id.btn_menu);
                menu.setVisibility(View.GONE);
                android.view.animation.Animation translate = AnimationUtils.loadAnimation(MapboxActivity.this, R.anim.slide_left);
                menu.startAnimation(translate);
                menu.setVisibility(View.VISIBLE);

            }
        });

        // Add on click listeners
        Button menuButton1 = (Button) findViewById(R.id.btn_menu1);
        menuButton1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sillyButton1) {
                    v.setBackground(MapboxActivity.this.getResources().getDrawable(R.drawable.btn_menu1_2));
                    showFancyDialog("You turned the ON to nothing!");
                    sillyButton1 = false;
                } else {
                    v.setBackground(MapboxActivity.this.getResources().getDrawable(R.drawable.btn_menu1_1));
                    showFancyDialog("You turned nothing ON!");
                    sillyButton1 = true;
                }
            }
        });

        Button menuButton2 = (Button) findViewById(R.id.btn_menu2);
        menuButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sillyButton2) {
                    v.setBackground(MapboxActivity.this.getResources().getDrawable(R.drawable.btn_menu2_2));
                    showFancyDialog("You turned OFF to nothing!");
                    sillyButton2 = false;
                } else {
                    v.setBackground(MapboxActivity.this.getResources().getDrawable(R.drawable.btn_menu2_1));
                    showFancyDialog("You turned nothing OFF!");
                    sillyButton2 = true;
                }
            }
        });

        CircleImageView profileButton = (CircleImageView) findViewById(R.id.btn_profile_image);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showFancyDialog("This circular button was developed to put a persisting face in your screen.");
            }
        });

        Button inboxButton = (Button) findViewById(R.id.btn_inbox);
        inboxButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button inbox = (Button)v;
                showFancyDialog("You have " + inbox.getText()+" new messages. You will never meet your zero-inbox goal.");
            }
        });

        Button arButton = (Button) findViewById(R.id.btn_AR);
        arButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Button inbox = (Button)v;
                new FancyAlertDialog.Builder(MapboxActivity.this)
                        .setTitle("Button Description")
                        .setBackgroundColor(Color.parseColor("#30AF4F"))  //Don't pass R.color.colorvalue
                        .setMessage("This button will show you the world of Augmented Reality")
                        .setNegativeBtnText("Not wanna go")
                        .setPositiveBtnBackground(Color.parseColor("#FF20D110"))  //Don't pass R.color.colorvalue
                        .setPositiveBtnText("Let's go")
                        .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                        .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                        .isCancellable(true)
                        .setIcon(R.drawable.bomb_marker, Icon.Visible)
                        .OnPositiveClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                startARActivity();
                            }
                        })
                        .OnNegativeClicked(new FancyAlertDialogListener() {
                            @Override
                            public void OnClick() {
                                Toast.makeText(getApplicationContext(),"Too bad",Toast.LENGTH_SHORT).show();
                            }
                        })
                        .build();
            }
        });

    }

    // Starts the AR activity
    private void startARActivity() {
        Intent i=new Intent(this, com.google.ar.core.examples.java.helloar.HelloArActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    @Override
    public void onStart() {
        super.onStart();
        mapView.onStart();

        // Start location layer plugin
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            //Request location updates:
            if(locationLayerPlugin!=null) {
                locationLayerPlugin.onStart();
            }
        }

    }

    @Override
    public void onResume() {
        super.onResume();

        // Also check location provider
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            //Request location updates:
            if(locationLayerPlugin!=null) {
                locationLayerPlugin.onStart();
                locationLayerPlugin.setLocationLayerEnabled(LocationLayerMode.TRACKING);
            }
        }

        // mapView
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();

        mapView.onPause();
    }

    @Override
    public void onStop() {
        super.onStop();
        mapView.onStop();
        // Also stop location layer
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            // Stop location layer and engine
            if(locationLayerPlugin != null)
                locationLayerPlugin.onStop();
            if (locationEngine != null) {
                locationEngine.removeLocationUpdates();
            }
        }
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
        if (locationEngine != null) {
            locationEngine.deactivate();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation? [prolly not needed]
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                new AlertDialog.Builder(this)
                        .setTitle("Location Permissions")
                        .setMessage("Provide location permissions")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //Prompt the user once explanation has been shown
                                ActivityCompat.requestPermissions(MapboxActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        permissionsManager.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // Deprecated?
//        switch (requestCode) {
//            case MY_PERMISSIONS_REQUEST_LOCATION: {
//                // If request is cancelled, the result arrays are empty.
//                if (grantResults.length > 0
//                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//
//                    // permission was granted, yay! Do the
//                    // location-related task you need to do.
//                    if (ContextCompat.checkSelfPermission(this,
//                            Manifest.permission.ACCESS_FINE_LOCATION)
//                            == PackageManager.PERMISSION_GRANTED) {
//
//                        //Request location updates:
//                        startLocationLayer();
//                    }
//
//                } else {
//
//                    // permission denied, boo! Disable the
//                    // functionality that depends on this permission.
//
//                }
//                return;
//            }
//
//        }
    }

    @SuppressWarnings( {"MissingPermission"})
    void startLocationEngine()
    {
        locationEngine = new LocationEngineProvider(this).obtainBestLocationEngineAvailable();
        locationEngine.setPriority(LocationEnginePriority.HIGH_ACCURACY);
        locationEngine.activate();

        locationEngine.addLocationEngineListener(this);
    }

    private void setCameraPosition(Location location) {
        mapBoxMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                new LatLng(location.getLatitude(), location.getLongitude()), 12));
    }

    void startLocationLayer()
    {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            startLocationEngine();

            locationLayerPlugin = new LocationLayerPlugin(mapView, mapBoxMap, locationEngine);
            locationLayerPlugin.setLocationLayerEnabled(LocationLayerMode.TRACKING);
        } else {
            checkLocationPermission();
        }
    }

    @Override
    @SuppressWarnings( {"MissingPermission"})
    public void onConnected() {
        locationEngine.requestLocationUpdates();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.d("TEST", "Location Arrived: " + location.toString());
        if (location != null) {
            setCameraPosition(location);

            moveMarker(location);
        }
    }

    private void moveMarker(Location location) {

        // Generate random pair
        Random random = new Random();
        double lat_disp = random.nextDouble(), long_disp = random.nextDouble();

        // Scale them to 0.0-0.02;
        lat_disp /= 50; long_disp /= 50;

        List<Feature> features = new ArrayList<>();
        features.add(Feature.fromGeometry(Point.fromCoordinates(new double[]{location.getLongitude()+long_disp, location.getLatitude()+lat_disp})));
        FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
        GeoJsonSource source = mapBoxMap.getSourceAs(MARKER_SOURCE);
        if (source != null) {
            source.setGeoJson(featureCollection);
        }
    }

    @Override
    public void onExplanationNeeded(List<String> permissionsToExplain) {

    }

    @Override
    public void onPermissionResult(boolean granted) {
        if (granted) {
            startLocationLayer();
        } else {
            Toast.makeText(getApplicationContext(),"Oups! Permission denied", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    void showFancyDialog(String description)
    {
        new FancyAlertDialog.Builder(this)
                .setTitle("Button Description")
                .setBackgroundColor(Color.parseColor("#30AF4F"))  //Don't pass R.color.colorvalue
                .setMessage(description)
                .setNegativeBtnText("Not Impressed")
                .setPositiveBtnBackground(Color.parseColor("#FF20D110"))  //Don't pass R.color.colorvalue
                .setPositiveBtnText("OK Great!")
                .setNegativeBtnBackground(Color.parseColor("#FFA9A7A8"))  //Don't pass R.color.colorvalue
                .setAnimation(com.shashank.sony.fancydialoglib.Animation.POP)
                .isCancellable(true)
                .setIcon(R.drawable.bomb_marker, Icon.Visible)
                .OnPositiveClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                        Toast.makeText(getApplicationContext(),"Good!",Toast.LENGTH_SHORT).show();
                    }
                })
                .OnNegativeClicked(new FancyAlertDialogListener() {
                    @Override
                    public void OnClick() {
                        Toast.makeText(getApplicationContext(),"Too bad",Toast.LENGTH_SHORT).show();
                    }
                })
                .build();
    }

    // Add bomb marker
    private void addMarkers() {
        List<Feature> features = new ArrayList<>();
    /* Source: A data source specifies the geographic coordinate where the image marker gets placed. */
        features.add(Feature.fromGeometry(Point.fromCoordinates(new double[] {39.14,21.17})));
        FeatureCollection featureCollection = FeatureCollection.fromFeatures(features);
        GeoJsonSource source = new GeoJsonSource(MARKER_SOURCE, featureCollection);
        mapBoxMap.addSource(source);
	/* Style layer: A style layer ties together the source and image and specifies how they are displayed on the map. */
        SymbolLayer markerStyleLayer = new SymbolLayer(MARKER_STYLE_LAYER, MARKER_SOURCE)
                .withProperties(
                        PropertyFactory.iconAllowOverlap(true),
                        PropertyFactory.iconImage(MARKER_NAME)
                );
        mapBoxMap.addLayer(markerStyleLayer);
    }

}